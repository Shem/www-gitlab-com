---
layout: markdown_page
title: "3rd Party Vendor Security Review process"
---

### Process Overview

Security Compliance performs security reviews of vendors for corporate  
consumption of services that may process GitLab data. As part of [New Vendor Evaluations](https://about.gitlab.com/handbook/finance/procurement/#1-new-vendor-evaluation) and [Vendor Contract Renewals](https://about.gitlab.com/handbook/finance/procurement/#2-existing-vendor-negotiation-for-renewals-and-true-ups), a security review will
will be required for all vendors and systems that will be processing, storing, and/or transmitting data 
[classified](https://about.gitlab.com/handbook/engineering/security/data-classification-policy.html) 
as Yellow or higher. To kick off this step, Security Compliance conducts a security review
of the vendor and the services and provides the resulting recommendation to the Security approver(s). Security approval is required in order to proceed to contracting.

## Scoped Labels
Below is a representation of the relationship between the Finance issue and Security Compliance review throughout the process and the corresponding labels used.

```mermaid
graph TD;
  1[Finance issue is created with `SecurityReview::NotStarted`]-->2[Is a vendor security review needed?];
  2[Is a vendor security review needed?]-->3[Yes. `SecurityReview::TriageNeeded` label];
  2[Is a vendor security review needed?]-->4[No. `SecurityReview::NotNeeded` label];
  4[No. `SecurityReview::NotNeeded` label]-->5[Finance issue continues];
  3[Yes. `SecurityReview::TriageNeeded` label]-->6[Request security documentation];
  6[Request security documentation]-->7[Review starts with `SecurityReview::Started` label];
  7[Review starts with `SecurityReview::Started` label]-->8[If documentation is not sufficient];
  8[If documentation is not sufficient]-->6[Request security documentation];
  7[Review starts with `SecurityReview::Started` label]-->9[Recommendation based on review & `SecurityReview::Completed` label];
  9[Recommendation based on review & `SecurityReview::Completed` label]-->5[Finance issue continues];
```

## Security Documentation Decision Tree

TBD
 
## Vendor Security Reports Review

 1. Finance issue
When a Finance issue is opened for new services, there is a section for `Security Approval`
that must be followed as part of the Contract and Approval Workflow. The instructions 
ask for security reports and certifications including SOC 2 Type 2. The label ~"SecurityReview::NotStarted" is applied by the requestor.

1. Security Compliance intake 
Security Compliance monitors the [Finance Security Review dashboard](https://gitlab.com/gitlab-com/finance/-/boards/1375804)
for new requests. The team engages with the business owner to determine the level
of data used, and applies one of the applicable scoped labels: ~"SecurityReview::TraigeNeeded"  ~"SecurityReview::NotNeeded" 

If the service or tool uses GitLab data classified as Yellow or above, Security Compliance will
open a [Compliance issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues?scope=all&utf8=✓&state=all&label_name[]=Vendor%20Security%20Review) 
using the [Vendor Security Report Review template](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=).
They will also request for the vendor to supply any available reports. The label ~"SecurityReview::Blocked" will be used in the Finance issue until the reports and/or completed 
questionnaire have been returned. Once received the label will then be updated to ~"SecurityReview::Started" 

1. Vendor Security Report Review
Reports and questionnaires returned by the vendor are reviewed for noted exceptions and gaps. Exceptions are recorded according 
to the instructions within the template and are summarized for the Security approver(s) as a comment in the original Finance issue. 
Information is requested from the vendor as needed to complete the review. Updates in the Finance issue must be kept current, 
along with the appropriate scoped label, such as ~"SecurityReview::Blocked" whenever waiting on the vendor. 
The label ~"SecurityReview::Completed" is used once the Security approval is made.

## Criteria
Security reviews are largely operating on a guidance basis where Security Compliance provides 
recommendations to Security after reviewing reports. As specifications and requirements are identified by
stakeholders, those will be added to this page as security guidance for sourcing.

## SLA
Security Compliance commits to a 3-business day SLA for completion when not waiting on input from the vendor. The scoped lables will be used to audit SLAs.
