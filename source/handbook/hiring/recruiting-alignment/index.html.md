---
layout: markdown_page
title: "Recruiting Alignment"
---

## Recruiter, Coordinator and Sourcer Alignment by Department

| Department                    | Recruiter       | Coordinator     |Sourcer     |
|--------------------------|-----------------|-----------------|-----------------|
| Board of Directors          | April Hoffbauer   | | |
| Executive          | Rich Kahn   | Bernadett Gal | Anastasia Pshegodskaya |
| Enterprise Sales, North America | Kelly Murdock   | Taharah Nix |Susan Hill/Loredana Iluca        |
| Commercial Sales,	NA/EMEA | Marcus Carter  | Ashley Jones|Susan Hill      |
| Commercial Sales, APAC | Simon Poon | Lea Hanopol | Viren Rana |
| Field Operations,	NA/EMEA/APAC | Kelly Murdock   | Taharah Nix |Susan Hill/Viren Rana - APAC        |
| Customer Success, NA/SA | Stephanie Garza  | Corinne Sapolu |Kanwal Matharu | 
| Customer Success, EMEA | Debbie Harris  | Bernadett Gal |Kanwal Matharu |
| Customer Success, APAC | Simon Poon | Lea Hanopol | Viren Rana |
| Federal Sales, Customer Success, Marketing | Stephanie Kellert   | Shiloh Barry |Viren Rana |
| Marketing, North America | Steph Sarff   | Shiloh Barry |Viren Rana |
| Marketing, EMEA | Sean Delea   | Kike Adio |Viren Rana |
| G&A | Maria Gore   | Heather Francisco |Loredana Iluca |
| Quality                   | Rupert Douglas                                          | Kike Adio        | Caesar Hsiao      |
| UX                        | Rupert Douglas                                          | Kike Adio        | Zsusanna Kovacs      |
| Technical Writing         | Rupert Douglas                                          | Kike Adio        | Zsusanna Kovacs      |
| Support                   | Cyndi Walsh                                             | Ashley Jones      | Alina Moise      |
| Support                   | Chantal Rollison                                            | Heather Francisco      | Alina Moise      |
| Security                  | Cyndi Walsh                                             | Ashley Jones      | Caesar Hsiao      |
| Infrastructure            | Matt Allen                                              | Emily Mowry      | Chris Cruz |
| Development - Dev         | Catarina Ferreira                                       | Corinne Sapolu        | Chris Cruz       |
| Development - Secure/Defend      | Liam McNally                                            | Lea Hanopol        | Alina Moise       |
| Development - Ops & CI/CD  | Eva Petreska                                            | Taharah Nix      | Zsuzsanna Kovacs      |
| Development - Enablement  | Trust Ogor                                              | Bernadett Gal        | Alina Moise       |
| Development - Growth      | Trust Ogor                                              | Bernadett Gal        | Alina Moise       |
| Engineering Leadership                | Liam McNally                                         | Lea Hanopol      |  Anastasia Pshegodskaya |
| Product Management  | Matt Allen                      | Emily Mowry |  Anastasia Pshegodskaya/Chris Cruz |
